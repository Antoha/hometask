package Testprog

class Tests extends GroovyTestCase{

    void testStr(){

        def param = SomeShifr.Gronsfeld.codding("some characters")
        assert SomeShifr.Gronsfeld.decodding(param) == "some characters"

        assert (/some text/ == 'some' + ' text')

        def str = ["elem":"inst"]
        def strn = str
        strn.elem = "isnt"
        assert strn == str
    }

    void testFunc(){
        double n = 10
        int x = Math.pow(Math.exp(1),Math.log(n))
        if (x == n){
            println "Yeah!"
            assert Math.pow(Math.exp(1),Math.log(n)) != n
            assert Math.pow(Math.exp(1),Math.log(n)) == n + 0.000000000000005
        }
        println("Test is successful")
    }

    void testClos(){
        def a =  Math.cos(0)
        def clos = {
            a = Math.log10(a)
        }
        assert clos() == 0
    }
}
